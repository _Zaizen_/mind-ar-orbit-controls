function makeLabelCanvas(baseWidth, size, name) {
    const borderSize = 2;
    const ctx = document.createElement('canvas').getContext('2d');
    const font = `${size}px bold sans-serif`;
    ctx.font = font;
    // measure how long the name will be
    const textWidth = ctx.measureText(name).width;
  
    const doubleBorderSize = borderSize * 2;
    const width = baseWidth + doubleBorderSize;
    const height = size + doubleBorderSize;
    ctx.canvas.width = width;
    ctx.canvas.height = height;
  
    // need to set font again after resizing canvas
    ctx.font = font;
    ctx.textBaseline = 'middle';
    ctx.textAlign = 'center';
  
    ctx.fillStyle = 'blue';
    ctx.fillRect(0, 0, width, height);
  
    // scale to fit but don't stretch
    const scaleFactor = Math.min(1, baseWidth / textWidth);
    ctx.translate(width / 2, height / 2);
    ctx.scale(scaleFactor, 1);
    ctx.fillStyle = 'white';
    ctx.fillText(name, 0, 0);
  
    return ctx.canvas;
  }
  
  window.addEventListener('DOMContentLoaded', function () {
    const sceneEl = document.querySelector('a-scene');
    console.log(sceneEl)
  
  
    // arReady event triggered when ready
    sceneEl.addEventListener("arReady", (event) => {
  
        const labelWidth = 150
        const size = 32
        const name = "A bear node"
        const canvas = makeLabelCanvas(labelWidth, size, name);
        const texture = new THREE.CanvasTexture(canvas);
        // because our canvas is likely not a power of 2
        // in both dimensions set the filtering appropriately.
        texture.minFilter = THREE.LinearFilter;
        texture.wrapS = THREE.ClampToEdgeWrapping;
        texture.wrapT = THREE.ClampToEdgeWrapping;
  
        const labelMaterial = new THREE.SpriteMaterial({
            map: texture,
            transparent: true,
        });
  
        const scene = sceneEl.object3D
        const racoon = document.querySelector('#racoon').object3D
        const bear = document.querySelector('#bear').object3D
  
  
        const labelBaseScale = 0.005;
        const label = new THREE.Sprite(labelMaterial);
  
        console.log(racoon)
        console.log(racoon.toJSON())
        /*
        console.log(bear.getObjectByName("MCH-hand_ik_rootL_09"))
        var handObj = bear.getObjectByName("MCH-hand_ik_rootL_09")*/
        bear.add(label)
  
        label.position.y = bear.position.y + size * labelBaseScale;
  
        label.scale.x = canvas.width * labelBaseScale;
        label.scale.y = canvas.height * labelBaseScale;
        
        const camera = document.querySelector('#camera').object3D.children[0]
        console.log(camera)
        
        console.log(sceneEl.canvas)
        const controls = new THREE.OrbitControls(sceneEl.camera, sceneEl.canvas);
        controls.target.set(0, 0, 0);
        controls.update();
        
  
    });
  })
  